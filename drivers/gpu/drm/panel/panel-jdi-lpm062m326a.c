/*
 * Copyright (C) 2018 SwtcR <swtcr0@gmail.com>
 * Copyright (C) 2023-2024 Azkali <a.ffcc7@gmail.com>
 *
 * Based on Sharp ls043t1le01 panel driver by Werner Johansson <werner.johansson@sonymobile.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <linux/backlight.h>
#include <linux/delay.h>
#include <linux/gpio/consumer.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/regulator/consumer.h>

#include <video/mipi_display.h>

#include <drm/drm_mipi_dsi.h>
#include <drm/drm_crtc.h>
#include <drm/drm_panel.h>
#include <drm/drm_modes.h>

/*! MIPI DCS Panel Private CMDs. */
#define MIPI_DCS_PRIV_SM_SET_COLOR_MODE 0xA0
#define MIPI_DCS_PRIV_SM_SET_REG_OFFSET 0xB0
#define MIPI_DCS_PRIV_SM_SET_ELVSS \
	0xB1 // OLED backlight tuning. Byte7: PWM transition time in frames.
#define MIPI_DCS_PRIV_SET_POWER_CONTROL 0xB1
#define MIPI_DCS_PRIV_SET_EXTC 0xB9 // Enable extended commands.
#define MIPI_DCS_PRIV_UNK_BD 0xBD
#define MIPI_DCS_PRIV_UNK_D5 0xD5
#define MIPI_DCS_PRIV_UNK_D6 0xD6
#define MIPI_DCS_PRIV_UNK_D8 0xD8
#define MIPI_DCS_PRIV_UNK_D9 0xD9

struct init_cmd {
	u8 cmd;
	int length;
	u8 data[64];
};

struct jdi_panel {
	struct drm_panel base;
	struct mipi_dsi_device *dsi;

	struct backlight_device *backlight;
	struct regulator *supply1;
	struct regulator *supply2;
	struct gpio_desc *reset_gpio;

	bool prepared;
	bool enabled;

	const struct drm_display_mode *mode;

	struct init_cmd *init_cmds;
	struct init_cmd *suspend_cmds;

	u8 display_id[3];
};

struct init_cmd init_cmds_0x10[] = {
	{ MIPI_DCS_PRIV_SET_EXTC, 3, { 0xFF, 0x83, 0x94 } },
	{ MIPI_DCS_PRIV_UNK_BD, 2, { 0x00, 0x0 } },
	{ MIPI_DCS_PRIV_UNK_D8, 24, { 0xAA, 0xAA, 0xAA, 0xEB, 0xAA, 0xAA,
				      0xAA, 0xAA, 0xAA, 0xEB, 0xAA, 0xAA,
				      0xAA, 0xAA, 0xAA, 0xEB, 0xAA, 0xAA,
				      0xAA, 0xAA, 0xAA, 0xEB, 0xAA, 0xAA } },
	{ MIPI_DCS_PRIV_UNK_BD, 2, { 0x01, 0x0 } },
	{ MIPI_DCS_PRIV_UNK_D8,
	  38,
	  { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF } },
	{ MIPI_DCS_PRIV_UNK_BD, 2, { 0x02, 0x0 } },
	{ MIPI_DCS_PRIV_UNK_D8,
	  14,
	  { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	    0xFF, 0xFF, 0xFF } },
	{ MIPI_DCS_PRIV_UNK_BD, 2, { 0x00, 0x00 } },
	{ MIPI_DCS_PRIV_UNK_D9, 2, { 0x06, 0x0 } },
	{ MIPI_DCS_PRIV_SET_EXTC, 3, { 0x00, 0x00, 0x00 } },
	{ MIPI_DCS_EXIT_SLEEP_MODE, 2, { 0x00, 0x0 } },
	{
		0xFF,
		180,
	},
	{ MIPI_DCS_SET_DISPLAY_ON, 2, { 0x00, 0x0 } },
	{
		0xFF,
		20,
	},
	{ MIPI_DCS_NOP, -1, { 0x00 } },
};

struct init_cmd suspend_cmds_0x10[] = {
	{ MIPI_DCS_SET_DISPLAY_OFF, 2, { 0x00, 0x0 } },
	{
		0xFF,
		50,
	},
	{ MIPI_DCS_PRIV_SET_EXTC, 3, { 0xFF, 0x83, 0x94 } },
	{ MIPI_DCS_PRIV_UNK_D5,
	  32,
	  { 0x19, 0x19, 0x19, 0x19, 0x19, 0x19, 0x19, 0x19, 0x19, 0x19, 0x19,
	    0x19, 0x19, 0x19, 0x19, 0x19, 0x19, 0x19, 0x19, 0x19, 0x19, 0x19,
	    0x19, 0x19, 0x19, 0x19, 0x19, 0x19, 0x19, 0x19, 0x19, 0x19 } },
	{ MIPI_DCS_PRIV_SET_POWER_CONTROL,
	  10,
	  { 0x41, 0x0F, 0x4F, 0x33, 0xA4, 0x79, 0xF1, 0x81, 0x2D, 0x00 } },
	{ MIPI_DCS_PRIV_SET_EXTC, 3, { 0x00, 0x00, 0x00 } },
	{ MIPI_DCS_ENTER_SLEEP_MODE, 2, { 0x00, 0x0 } },
	{
		0xFF,
		50,
	},
	{ MIPI_DCS_NOP, -1, { 0x00 } },
};

static inline struct jdi_panel *to_jdi_panel(struct drm_panel *panel)
{
	return container_of(panel, struct jdi_panel, base);
}

static void jdi_panel_detect(struct jdi_panel *jdi)
{
	int ret;
	jdi->init_cmds = NULL;
	jdi->suspend_cmds = NULL;

	memset(jdi->display_id, 0, sizeof(jdi->display_id));

	ret = mipi_dsi_dcs_read(jdi->dsi, MIPI_DCS_GET_DISPLAY_ID,
				jdi->display_id, 3);
	if (ret < 0) {
		dev_err(&jdi->dsi->dev, "failed to read panel ID: %d\n", ret);
	} else {
		dev_info(&jdi->dsi->dev, "display ID[%d]: %02x %02x %02x\n",
			 ret, jdi->display_id[0], jdi->display_id[1],
			 jdi->display_id[2]);
	}

	switch (jdi->display_id[0]) {
	case 0x10:
		dev_info(&jdi->dsi->dev,
			 "using init/suspend sequence for ID 0x10\n");
		jdi->init_cmds = init_cmds_0x10;
		jdi->suspend_cmds = suspend_cmds_0x10;
		break;
	default:
		dev_info(&jdi->dsi->dev, "unknown display, no extra suspend\n");
		break;
	}

	msleep(20);
}

static int jdi_mipi_dsi_dcs_cmds(struct init_cmd *cmds, struct jdi_panel *jdi)
{
	int ret = 0;

	while (cmds && cmds->length != -1) {
		if (cmds->cmd == 0xFF)
			msleep(cmds->length);
		else {
			ret = mipi_dsi_dcs_write(jdi->dsi, cmds->cmd,
						 cmds->data, cmds->length);
			if (ret < 0) {
				dev_err(&jdi->dsi->dev,
					"failed to write dsi_cmd: %d error: %d\n",
					cmds->cmd, ret);
				return ret;
			}
		}
		cmds++;
	}

	return ret;
}

static int jdi_panel_init(struct jdi_panel *jdi)
{
	struct mipi_dsi_device *dsi = jdi->dsi;
	struct device *dev = &jdi->dsi->dev;
	int ret;

	dsi->mode_flags |= MIPI_DSI_MODE_LPM;

	ret = mipi_dsi_set_maximum_return_packet_size(dsi, 3);
	if (ret < 0) {
		dev_err(dev, "failed to set maximum return packet size: %d\n",
			ret);
		return ret;
	}

	jdi_panel_detect(jdi);

	ret = jdi_mipi_dsi_dcs_cmds(jdi->init_cmds, jdi);
	if (ret < 0)
		return ret;

	ret = mipi_dsi_dcs_set_column_address(dsi, 0, jdi->mode->hdisplay - 1);
	if (ret < 0) {
		dev_err(dev, "failed to set page address: %d\n", ret);
		return ret;
	}

	ret = mipi_dsi_dcs_set_page_address(dsi, 0, jdi->mode->vdisplay - 1);
	if (ret < 0) {
		dev_err(dev, "failed to set column address: %d\n", ret);
		return ret;
	}

	ret = mipi_dsi_dcs_set_tear_on(dsi, MIPI_DSI_DCS_TEAR_MODE_VBLANK);
	if (ret < 0) {
		dev_err(dev, "failed to set vblank tear on: %d\n", ret);
		return ret;
	}

	ret = mipi_dsi_dcs_set_pixel_format(dsi, MIPI_DCS_PIXEL_FMT_24BIT);
	if (ret < 0) {
		dev_err(dev, "failed to set pixel format: %d\n", ret);
		return ret;
	}

	return 0;
}

static int jdi_panel_enable(struct drm_panel *panel)
{
	struct jdi_panel *jdi = to_jdi_panel(panel);

	if (jdi->enabled)
		return 0;

	backlight_enable(jdi->backlight);

	jdi->enabled = true;

	return 0;
}

static int jdi_panel_disable(struct drm_panel *panel)
{
	struct jdi_panel *jdi = to_jdi_panel(panel);

	if (!jdi->enabled)
		return 0;

	backlight_disable(jdi->backlight);

	jdi->enabled = false;

	return 0;
}

static int jdi_panel_unprepare(struct drm_panel *panel)
{
	struct jdi_panel *jdi = to_jdi_panel(panel);
	int ret;

	if (!jdi->prepared)
		return 0;

	jdi->dsi->mode_flags &= ~MIPI_DSI_MODE_LPM;

	ret = jdi_mipi_dsi_dcs_cmds(jdi->suspend_cmds, jdi);
	if (ret < 0)
		dev_err(&jdi->dsi->dev, "failed to write suspend cmds: %d\n",
			ret);

	if (jdi->reset_gpio)
		gpiod_set_value(jdi->reset_gpio, 0);

	msleep(10);
	regulator_disable(jdi->supply2);
	msleep(10);
	regulator_disable(jdi->supply1);

	jdi->prepared = false;

	return 0;
}

static int jdi_panel_prepare(struct drm_panel *panel)
{
	struct jdi_panel *jdi = to_jdi_panel(panel);
	struct device *dev = &jdi->dsi->dev;
	int ret;

	if (jdi->prepared)
		return 0;

	ret = regulator_enable(jdi->supply1);
	if (ret < 0)
		return ret;
	msleep(10);

	ret = regulator_enable(jdi->supply2);
	if (ret < 0)
		goto poweroff;
	msleep(10);

	if (jdi->reset_gpio) {
		gpiod_set_value(jdi->reset_gpio, 0);
		msleep(10);
		gpiod_set_value(jdi->reset_gpio, 1);
		msleep(60);
	}
	jdi->dsi->mode_flags |= MIPI_DSI_MODE_LPM;

	ret = jdi_panel_init(jdi);
	if (ret < 0) {
		dev_err(dev, "failed to init panel: %d\n", ret);
		goto reset;
	}

	jdi->prepared = true;

	return 0;

reset:
	if (jdi->reset_gpio)
		gpiod_set_value(jdi->reset_gpio, 0);
	regulator_disable(jdi->supply2);

poweroff:
	regulator_disable(jdi->supply1);
	return ret;
}

static const struct drm_display_mode default_mode = {
	.clock = 78000,
	.hdisplay = 720,
	.hsync_start = 720 + 136,
	.hsync_end = 720 + 136 + 72,
	.htotal = 720 + 136 + 72 + 72,
	.vdisplay = 1280,
	.vsync_start = 1280 + 10,
	.vsync_end = 1280 + 10 + 2,
	.vtotal = 1280 + 10 + 1 + 9,
	.width_mm = 77,
	.height_mm = 137,
};

static int jdi_panel_get_modes(struct drm_panel *panel,
			       struct drm_connector *connector)
{
	struct drm_display_mode *mode;
	struct jdi_panel *jdi = to_jdi_panel(panel);
	struct device *dev = &jdi->dsi->dev;

	mode = drm_mode_duplicate(connector->dev, &default_mode);
	if (!mode) {
		dev_err(dev, "failed to add mode %ux%ux@%u\n",
			default_mode.hdisplay, default_mode.vdisplay,
			drm_mode_vrefresh(&default_mode));
		return -ENOMEM;
	}

	drm_mode_set_name(mode);
	drm_mode_probed_add(connector, mode);

	connector->display_info.width_mm = default_mode.width_mm;
	connector->display_info.height_mm = default_mode.height_mm;

	return 1;
}

static const struct drm_panel_funcs jdi_panel_funcs = {
	.prepare = jdi_panel_prepare,
	.unprepare = jdi_panel_unprepare,
	.enable = jdi_panel_enable,
	.disable = jdi_panel_disable,
	.get_modes = jdi_panel_get_modes,
};

static int jdi_panel_add(struct jdi_panel *jdi)
{
	struct device *dev = &jdi->dsi->dev;
	struct device_node *np;

	jdi->mode = &default_mode;

	jdi->supply1 = devm_regulator_get(dev, "vdd1");
	if (IS_ERR(jdi->supply1))
		return PTR_ERR(jdi->supply1);

	jdi->supply2 = devm_regulator_get(dev, "vdd2");
	if (IS_ERR(jdi->supply2))
		return PTR_ERR(jdi->supply2);

	jdi->reset_gpio = devm_gpiod_get(dev, "reset", GPIOD_OUT_LOW);
	if (IS_ERR(jdi->reset_gpio)) {
		dev_err(dev, "cannot get reset-gpios %ld\n",
			PTR_ERR(jdi->reset_gpio));
		jdi->reset_gpio = NULL;
	} else {
		gpiod_set_value(jdi->reset_gpio, 0);
	}

	np = of_parse_phandle(dev->of_node, "backlight", 0);
	if (np) {
		jdi->backlight = of_find_backlight_by_node(np);
		of_node_put(np);

		if (!jdi->backlight)
			return -EPROBE_DEFER;
	}

	drm_panel_init(&jdi->base, &jdi->dsi->dev, &jdi_panel_funcs,
		       DRM_MODE_CONNECTOR_DSI);

	drm_panel_add(&jdi->base);

	return 0;
}

static void jdi_panel_del(struct jdi_panel *jdi)
{
	if (jdi->base.dev)
		drm_panel_remove(&jdi->base);

	if (jdi->backlight)
		put_device(&jdi->backlight->dev);
}

static int jdi_panel_probe(struct mipi_dsi_device *dsi)
{
	struct jdi_panel *jdi;
	int ret;

	dsi->lanes = 4;
	dsi->format = MIPI_DSI_FMT_RGB888;
	dsi->mode_flags = MIPI_DSI_MODE_VIDEO |
			  MIPI_DSI_CLOCK_NON_CONTINUOUS |
			  MIPI_DSI_MODE_NO_EOT_PACKET;

	jdi = devm_kzalloc(&dsi->dev, sizeof(*jdi), GFP_KERNEL);
	if (!jdi)
		return -ENOMEM;

	mipi_dsi_set_drvdata(dsi, jdi);

	jdi->dsi = dsi;

	ret = jdi_panel_add(jdi);
	if (ret < 0)
		return ret;

	ret = mipi_dsi_attach(dsi);
	if (ret < 0) {
		jdi_panel_del(jdi);
		return ret;
	}

	return 0;
}

static void jdi_panel_remove(struct mipi_dsi_device *dsi)
{
	struct jdi_panel *jdi = mipi_dsi_get_drvdata(dsi);
	int ret;

	ret = jdi_panel_disable(&jdi->base);
	if (ret < 0)
		dev_err(&dsi->dev, "failed to disable panel: %d\n", ret);

	ret = mipi_dsi_detach(dsi);
	if (ret < 0)
		dev_err(&dsi->dev, "failed to detach from DSI host: %d\n", ret);

	jdi_panel_del(jdi);
}

static void jdi_panel_shutdown(struct mipi_dsi_device *dsi)
{
	struct jdi_panel *jdi = mipi_dsi_get_drvdata(dsi);
	jdi_panel_disable(&jdi->base);
}

static const struct of_device_id jdi_of_match[] = {
	{
		.compatible = "jdi,lpm062m326a",
	},
	{ /* sentinel */ }
};
MODULE_DEVICE_TABLE(of, jdi_of_match);

static struct mipi_dsi_driver jdi_panel_driver = {
	.driver = {
		.name = "panel-jdi-lpm062m326a",
		.of_match_table = jdi_of_match,
	},
	.probe = jdi_panel_probe,
	.remove = jdi_panel_remove,
	.shutdown = jdi_panel_shutdown,
};
module_mipi_dsi_driver(jdi_panel_driver);

MODULE_AUTHOR("SwtcR <swtcr0@gmail.com>");
MODULE_DESCRIPTION("JDI LPM062M326A (720x1280) panel driver");
MODULE_LICENSE("GPL v2");
